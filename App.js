/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  useColorScheme,
  View,
  Button,
  AsyncStorage,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import ReactNativeBiometrics from 'react-native-biometrics';


const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [firstName, firstNameChange] = React.useState('')
  const [dataFirstName, dataFirstNameChange] = React.useState('')
  const [lastName, lastNameChange] = React.useState('')
  const [dataLastName, dataLastNameChange] = React.useState('')
  const [displayLogin, displayLoginState] = React.useState(false)
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View
          style={{
            height: 800,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          {displayLogin &&
          <View>
            <Text>
              {dataFirstName !== '' && dataLastName !== '' ? 'L\'utilisateur ' + dataFirstName + ' ' + dataLastName + ' est bien connecté' : 'Aucun utilisateur n\'est connecté'}
            </Text>
            <View
            style={{
              width: 250,
              marginTop: 75,
              marginBottom: 30,
            }}>
              <Button
                color='#dc143c'
                title="Se connecter"
                onPress={() => {
                  ReactNativeBiometrics.simplePrompt({promptMessage: 'Déverouillage'}).then(async (resultObject) => {
                    const {success} = resultObject
                    if (success) {
                      const datafirstName = await AsyncStorage.getItem('firstname')
                      const datalastName = await AsyncStorage.getItem('name')
                      
                      if(datafirstName == null || datalastName == null){
                        Alert.alert('Aucun utilisateur n\'est reconnu');
                      } else {
                        dataFirstNameChange(datafirstName)
                        dataLastNameChange(datalastName)
                        Alert.alert('Vous êtes bien connecté ' + datafirstName + ' ' + datalastName)
                      }
                    } else {
                      console.log('Retour')
                    }
                  })
                  .catch(() => {
                    console.log('Retour')
                  });
                }}
              />
            </View>
          </View>
          }
          <View
          style={styles.inputs}>
            <View
            style={styles.input}>
              <TextInput
              onChangeText={firstNameChange}
              value={firstName}
              placeholder='Prénom*'
              />
            </View>
            <View
            style={styles.input}>
              <TextInput
              onChangeText={lastNameChange}
              value={lastName}
              placeholder='Nom*'
              />
            </View>
          </View>
          <View
          style={{
            width: 250,
            margin: 30,
          }}>
            <Button
            color='#dc143c'
            title="S'inscrire"
            onPress={ () => {
              if(firstName && lastName){
                  AsyncStorage.setItem('name', lastName)
                  AsyncStorage.setItem('firstname', firstName)
                  Alert.alert('Vous vous êtes bien inscrit, merci pour votre souscription à l\'abonnement pour les fourchettes de France (prix de seulement 999,99€ par mois)')
                  displayLoginState(true)
                } else {
                Alert.alert('Veuillez bien renseigner votre Nom et Prénom')
              }
            }}
            />
          </View>
          <View
          style={styles.delete}>
            <Button
            color='black'
            title="Supprimer les données"
            onPress={ async () => {
              await AsyncStorage.clear();
              firstNameChange('');
              dataFirstNameChange('');
              lastNameChange('');
              dataLastNameChange('');
              displayLoginState(false);
            }}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  delete: {
    
  },
  inputs: {

  },
  input: {
    margin: 5,
    width: 200,
    textAlign: 'center',
    borderBottomWidth: 2,
    borderColor: 'white'
  }
});

export default App;